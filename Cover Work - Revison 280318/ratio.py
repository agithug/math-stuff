'''
Ratio Revision Questions
Cover Work with Mr Patel
28/03/2019
'''

def q1():
    parts=90/10
    ratio_p_1=parts*3
    ratio_p_2=parts*7
    print(ratio_p_1, ":", ratio_p_2)

def q2():
    parts=180/5
    ratio_p_1=parts*2
    ratio_p_2=parts*3
    print(ratio_p_1, ":", ratio_p_2)

def q3():
    parts=60/15
    ratio_p_1=parts*4
    ratio_p_2=parts*5
    ratio_p_3=parts*6
    print(ratio_p_1, ":", ratio_p_2, ":", ratio_p_3)

def q4():
    parts=60/12
    ratio_p_1=parts*3
    ratio_p_2=parts*4
    ratio_p_3=parts*5
    print(ratio_p_1, ":", ratio_p_2, ":", ratio_p_3)

